-- Получить название бренда с идентификатором 3
SELECT name FROM brand WHERE id=3;

-- Получить первые два типа товара (сортировка по id)
SELECT * FROM product_type ORDER BY id ASC LIMIT 2;						-- в прямом порядке
SELECT * FROM product_type ORDER BY id DESC LIMIT 2;					-- в обратном порядке

-- Получить все категории товаров со скидкой <10% и отсортировать их по названию
SELECT * FROM category WHERE discount<10 ORDER BY name ASC;				-- в прямом порядке
SELECT * FROM category WHERE discount<10 ORDER BY name DESC;			-- в обратном	 порядке